#!/bin/bash

# stop my spelling mistakes!
set -u


# A menu driven shell script sample template 
# adapted from from //bash.cyberciti.biz/guide/Menu_driven_scripts

#-----------------------------------
# Step: declarations
# ----------------------------------

# FIND=/usr/bin/find


MAIN_MENU=1
FIND_TYPE_MENU=2
FIND_FS_TYPE=3
SET_ACTION_MENU=4

current_menu="$MAIN_MENU"
user_choice=0


GREEN='\e[92m'
REDTEXT='\e[31m'
RED='\033[0;41;30m'
STD='\033[0;0;39m'


##	recording values
start_point=""
start_point_test=""
name_string=""
find_type=""
group_name_string=""
fs_flag=""
include_symbolic=false
maxDepth="0"
action_basic=""

##   building things
find_cmd_line=""

#-----------------------------------
# Step: functions for output
# ----------------------------------
pause(){
 	echo -e "\n${GREEN}Press [Enter] key to continue...${STD}"
 	read -p "" throwAway
}


report_value_been_set(){
	
	
	echo  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	echo start_point="$start_point"
	echo start_point_test="$start_point_test"
	echo find_type="$find_type"
	echo group_name_string="$group_name_string"
	echo name_string="$name_string"
	echo fs_flag="$fs_flag"
	echo include_symbolic="$include_symbolic"
	echo maxDepth="$maxDepth"
	echo action_basic="$action_basic"
	echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	echo
}

reset_type_variables(){
	find_type=""
	group_name_string=""

}



# execute the find cmd built in the build find cmd function
run_find_cmd_line(){
	
	# execute with eval
	echo -e "${GREEN}\n ~ START ~~~~~~~~~~~~~~~~~~~~~~~~~~~  \n ${STD}"
	#### eval "find $find_cmd_line"
	exit_code=$?
	echo -e "${GREEN}\n ~END ~~~~~~~~~~~~~~~~~~~~~~~~~~~ ${STD}"
	# echo "exit_code is: $exit_code"

	# report success
	if [ "$exit_code" = 0 ]
	then 
		echo -e "Command successful!"
	else
		echo -e "${REDTEXT} Cound not execute the find cmd: ${STD}"
	fi

	# wait for client
	echo "Your find cmd was: $find_cmd_line"
	pause

}

build_find_cmd_line(){

	###	recorded values
	##	gstart_point="~"
	##	gstart_point_test="1"
	##	gname_string="dumb"
	##	gfind_type="f"
	##	group_name_string=""
	##fs_flag=""
	
	#-$find_type 

	####   TESTing things
	#   echo "name_string= -$name_string-"
	#   echo "start_point_test: $start_point_test" #="1"
	#	echo "in Start point"
	#	echo "in name point"
	
	# build 
	
	# must reset cmd line to start
	find_cmd_line=""
	
# symbolic links
	if [ "$include_symbolic" = "0" ] 
	then
		include_symbolic="-P"
		# Never follow symbolic links
	else
		include_symbolic="-L"
		# Follow symbolic links.
	fi
	

#start point  
	if [ ! "$start_point" = "" ] 
	then
		find_cmd_line="$start_point"
	fi

# maxDepth - if int provided
	if [ ! "$maxDepth" = "0" ]
	then
		find_cmd_line="$find_cmd_line -maxdepth $maxDepth"
	fi
	
# fstype - if a string was provided
	if [ ! "$fs_flag" = "" ]
	then
		find_cmd_line="$find_cmd_line -fstype \"$fs_flag\""
	fi

# name - if a string was provided
	if [ ! "$name_string" = "" ]
	then
		find_cmd_line="$find_cmd_line -name \"$name_string\""
	fi

	
# find_type - if set to d or f
	if [ "$find_type" = "d" ] || [ "$find_type" = "f" ] 
	then
		find_cmd_line="$find_cmd_line -type $find_type"
	fi


# action_basic - if a string was provided
	if [ ! "$action_basic" = "" ]
	then
		find_cmd_line="$find_cmd_line -${action_basic}"
	fi


}



set_start_point(){

	read -p "Type your starting point:" start_point
		
	# set start point test 
	if [ -d "${start_point}"  ]
	then
		start_point_test=1
	else
		start_point_test=0
	fi
} 

set_name_string(){
	read -p "Type your name (search) string:" name_string
}

set_group_name(){
	read -p "Type the group name to search within  (numeric group ID allowed)." group_name_string
}


set_maxDepth(){
	
	local maxDepth1="0"
	read -p "Type in a positive integer for the maximum depth (numeric)." maxDepth1
	
	
	if [[ "$maxDepth1" =~ ^[0-9]+$ ]]
    then
		maxDepth="$maxDepth1"
	else
        
		echo -e "${RED}Error...${STD} ${REDTEXT} Sorry integers only ${STD}" && sleep 5
		
	fi
	
	
}



## include_symbolic="0"
toggle_symbolic_links(){
	if [ $include_symbolic = "0" ]
	then 
		echo including
		
		include_symbolic="1"
	else
		echo excluding
		
		include_symbolic="0"
	fi 
	sleep 2
	
}



# ----------------------------------------------
#  Read user input and process
# ----------------------------------------------

# read input from the keyboard 
# select processor based on menu level

read_options(){
	
	# get user choice
	read -p "Enter choice :" user_choice
	case "$current_menu" in
		$MAIN_MENU      ) menu_1_process ;;
		$FIND_TYPE_MENU ) menu_2_process ;;
		$FIND_FS_TYPE   ) menu_3_process ;;
		$SET_ACTION_MENU) menu_4_process ;;
		
		*) echo -e "${RED}Error...${STD}" && sleep 2 ;;
	esac
}



 
# ----------------------------------------------
#  menus
# ----------------------------------------------

##    to alternate between menus
show_menu(){

	# echo "DEBUG: current_menu : $current_menu"
	case "$current_menu" in
		$MAIN_MENU) 	  menu_1_show ;;
		$FIND_TYPE_MENU)  menu_find_type_show ;;
		$FIND_FS_TYPE) 	  menu_find_fs_show ;;
		$SET_ACTION_MENU) menu_set_action_show ;;
		
		*)   echo "error: menu display current menu is: $current_menu">$2 
			 exit ;;
	esac
}


# OPENING MENU 
# function to display menu
menu_1_show() {

	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " F I N D E R - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. Set base directory to start"
	echo "2. Choose an item type "
	echo "3. Search string (item name)"
	echo "4. Add file system type to find"
	echo "5. Toggle search symbolic links"
	echo "6. Include a maximum depth"
	echo "7. Action commands"
	
	echo "~~"
	echo "9. RUN FIND!"
	echo "~~"
	echo "0. EXIT -(quit program)"
	echo ""
}

# opening menu case select
menu_1_process(){
	
	case "$user_choice" in
		1) set_start_point ;;
		2) current_menu="$FIND_TYPE_MENU" ;;
		3) set_name_string ;;
		4) current_menu="$FIND_FS_TYPE" ;;
		5) toggle_symbolic_links ;;
		6) set_maxDepth ;;
		7) current_menu=$SET_ACTION_MENU ;;
		
		9) 
			build_find_cmd_line
			run_find_cmd_line 
			;;
		
		0) exit 0;;
		*) echo -e "${RED}Error...${STD}" && sleep 2 ;;
	esac
}




menu_find_type_show(){


	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " choose type"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "d. directory"
	echo "f. regular file"
	echo "g. group gname File belongs (numeric group ID allowed)."
	echo "s. fstype file system type"
	echo "~~"
	echo "0. EXIT -(quit program)"
	echo ""

} 


# find type menu 
menu_2_process(){

	#  forget previous type selections
	reset_type_variables
	
	case "$user_choice" in
		d|f) 
			find_type="$user_choice"
			current_menu="$MAIN_MENU"
			;;
		g) 
			set_group_name
			current_menu="$MAIN_MENU"
			;;
		s) current_menu="$FIND_FS_TYPE" ;;
		
		0) current_menu="$MAIN_MENU";;
		*) echo -e "${RED}Error...${STD}" && sleep 2 ;;
	esac
}


menu_find_fs_show(){


	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " choose fstype"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. jfs"
	echo "2. nfs"
	echo "3. 4.2"
	echo "4. ext4"
	echo "remove fstype"
	echo "~~"
	echo "0. RETURN -(return )"
	echo ""
	

} 

### fs_flag=""
# actions menu 
menu_3_process(){
	
	case "$user_choice" in	
		1) fs_flag="a" ;;
		2) fs_flag="nfs" ;;
		3) fs_flag="4.2" ;;
		4) fs_flag="ext4" ;;
		5) fs_flag="" ;;

		0) current_menu="$MAIN_MENU";;
		*) echo -e "${RED}Error...${STD}" && sleep 2 ;;
	esac
	
	current_menu="$MAIN_MENU"

	
}

menu_set_action_show(){


	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " choose fstype"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. delete"
	echo "2. print"
	echo "3. print0"
	echo "~~"
	echo "9. clear action"
	echo "~~"
	echo "0. RETURN -(return )"
	echo ""
	

} 

### action_basic=""
# find fstype menu 
menu_4_process(){
	
	case "$user_choice" in	
		1) action_basic="delete" ;;
		2) action_basic="print" ;;
		3) action_basic="print0" ;;
		9) action_basic="" ;;

		0) current_menu="$MAIN_MENU";;
		*) echo -e "${RED}Error...${STD}" && sleep 2 ;;
	esac
	
	current_menu="$MAIN_MENU"

	
}
 
# ----------------------------------------------
#  Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------

## TRAPS
##### TODO: the working trap
trap "" SIGINT SIGQUIT SIGTSTP
##### TODO: 
##### demo of function in trap: trap "rm -f $lockfile; exit" INT TERM EXIT


# -----------------------------------
#  Main logic 
# ------------------------------------



# infinite loop
while true
do
	clear
	report_value_been_set
	show_menu
	read_options
done
