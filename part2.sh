#!/bin/bash

# anthony brown s3460996

# this script reports system information
# use the flags <cptfm>

# fuctions
# PS=/bin/ps
# NPROC=/bin/nproc
# WHOAMI=/bin/whoami
# LSOF=/bin/lsof

FLAG_OPTIONS="cptfm"

cpu_information() {
	
	# this function provides count of cpu
	# uses nproc
	echo "======================="
	echo "Count of CPUs in the system:"
	echo -e "\t $(nproc)" 
	echo "======================="
}


current_process_niceness() {
	
	# this function prints the nice value for the current process
	# this is done using ps commands
	local pid_value
	local ni_value
	local comm_value

	# get the pid for the current process
	pid_value=$$

	# get the nice value using the PID 
	ni_value="$(ps -q $pid_value o ni=)"

	# get the script name using PID, for presentation
	comm_value="$(ps -q $pid_value o comm=)"
		
	echo "======================="
	echo "The nice number for the current process:"
	echo -e "process name:  \t $comm_value"
	echo -e "The nice value:\t $ni_value"
	echo "======================="
}

current_user_total_processes() {
	
	# counts the total processes for th current user
	# uses whoami to get the user name
	# uses ps to  return to count of processes

	local total_processes
	total_processes="$(ps -U "$(whoami)" heo pid | wc -l)"
	
	echo "======================="
	echo "The total process for the current user is:"
	echo -e "\t $total_processes"
	echo "======================="
}

current_user_file_descriptors() {
	
	# this function gets the number of file descripters that
	# the current user has open
	# uses whomai to return the users name
	# uses lsof to find the matching file descripters

	local users_file_descripters 
	users_file_descripters="$(lsof -u "$(whoami)" | wc -l)"
	
	echo "======================="
	echo "the number of open file descriptors for"
	echo "the current user is:"
	echo -e "\t $users_file_descripters"
	echo "======================="
}


maximum_default_file_descripters() {
	
	# this function gets the default max number of file decripters 
	# that is currently set
	# uses cat to interogate the source file directly

	local max_file_descripters

	max_file_descripters="$(cat /proc/sys/fs/file-max)"
	echo "======================="
	echo "The maximum number of file descriptors that"
	echo "can be opened by a process is:"
	echo -e "\t $max_file_descripters"
	echo "======================="
}

bad_flagging() {
	
	# meesage the caller that no flag was recieved
	echo "script requires a flag -<cptfm>"
}


# test correct number of flags
if [[ $# -ne 1 ]]; then
	echo "This command requires flags:"
	echo "usage: $0 <-$FLAG_OPTIONS>" >&2
    	exit 1
fi



########################################
### main 

# process flags - deals with bad flags
while getopts $FLAG_OPTIONS opt; do
	case $opt in
		c)
			echo
			cpu_information
			echo >&2
			;;
		p)
			echo
			current_process_niceness
			echo >&2
			;;
		t)
			echo
			current_user_total_processes
			echo >&2
			;;
		f)
			echo
			current_user_file_descriptors
			echo >&2
			;;
		m)
			echo
			maximum_default_file_descripters
			echo >&2
			;;
		\?)
			bad_flagging
			echo >&2
			;;
	esac
done








