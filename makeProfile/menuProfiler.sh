#!/bin/bash

# stop my spelling mistakes!
set -u


# pretty text
GREEN='\e[92m'
REDTEXT='\e[31m'
RED='\033[0;41;30m'
STD='\033[0;0;39m'

#test
#ps
#grep


# menus
START=1
STRING_TO_PROCESS=2
STRING_MATCHS=3
CONFIRM_CHOICE=4
SELECT_PROFILE=5
DISPLAY_PROFILE=6
EXIT_REQUEST=7


#select profile
MEMORY=1
CPU=2


console_message="please choose"
countRecords=0   # must start at zero
name_string=""
## old search_table=""
exit_script=0   #must start at zero
user_choice=""
profile_watch=0

# the item to profile
p_name=""
p_pid=""

#-----------------------------------
# Step: functions for output
# ----------------------------------

# pause with propt
pause(){
 	echo -e "\n${GREEN}Press [Enter] key to continue...${STD}"
 	read -p "" throwAway
}


# get the name of the process
request_process_name(){
	
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " P R O V I D E  P O C E S S  N A M E"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo ""
	
	console_message="enter name of process to watch :"
	
	### OLD  read -p "Type the name of the proces to profile (search) string (part name is OK):" name_string}
}


# calls two functions to get the process to 1
ensure_one_process_selected(){
	
	make_matched_items_table
	decide_menu_on_counts
}

# look for requested process in ps processses
make_matched_items_table(){ 

  ## store the results in a tempFile.tmp
	
	echo "DEV name set here"
	name_string='notepad'

	

	
	### not working very well
	### search_table=$(ps -A | grep $name_string)
	### echo -e "search_table: "
	### echo "$search_table"
	### "$search_table" | awk "//{printf\"%s.  %s\n\", NR, $4 }"
	
	
	# make a temp file to work with
	ps -A | grep $name_string > tempFile.tmp
	countRecords=$( < tempFile.tmp wc -l)
	
	echo "DEV - count records: file "
	echo "count: $countRecords"
	
	#make the match table
	# so it can be displayed to inform the client
	
	# TODO moved this out of the  if statement
	menu_table=$( awk "//{printf\"%s.  %s\n\", NR, \$4 }" tempFile.tmp)
	
	#if [[ "$countRecords" -gt 1 ]]
	#then
		###echo menu_table is:
		###echo "$menu_table"
	#fi

	current_menu="$STRING_TO_PROCESS"

}

decide_menu_on_counts(){
	
	if [[ "$countRecords" -eq 1 ]]
	then
		# goto confirm match
		current_menu=$STRING_TO_PROCESS
	
	else
		if [[  "$countRecords" -gt 9 ]]
		then 
			
			echo "$menu_table"
			echo "more than ten records, please refine your search" 
			
			#GO back and loop
			current_menu=$START
			
		else
			if [[  "$countRecords" -lt 1 ]]
			then 
				echo "no matchs, please refine your search"
				#GO back and loop
				current_menu=$START
				
			else
				## right number of returns to go to the 
				## select options menu
				#GOTO make_the_Menu
				current_menu=$STRING_TO_PROCESS
				
			fi				
		fi
	fi


}


# display processes that match string
menu_show_find_process(){

	
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " C H O O S E   F R O M  M A T C H E S"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo ""

	echo "$menu_table"
	
	echo "~~"
	echo "b.  search once more "
	echo "~~"

}


#  extracts the item from the menu table
menu_process_find_process(){
	# match the choice to the menu_table
	
	
	if [[ "$user_choice" == "b" ]]
	then
		echo "no matchs, please refine your search"
		#GO back and loop
		current_menu=$START
	else
		# use the table and make a match
		## 
		
		if [ "$user_choice" -le "$countRecords" ] &&  [ "$user_choice" -gt 0 ]  
		then

			p_name=$( awk "//{printf\"%s.  %s\n\", NR == \"$user_choice\", \$4 }" tempFile.tmp)
			p_pid=$( awk "//{printf\"%s.  %s\n\", NR == \"$user_choice\", \$1 }" tempFile.tmp)

			 echo p_name: "$p_name"
			 echo p_pid: "$p_pid"
			pause
			
			current_menu="$CONFIRM_CHOICE"
		
		else
			echo "${REDTEXT}Your choice is outside the range ${STD}"
			
			current_menu="$START"
		fi
	fi
	
}


# display confirm propt
menu_show_confirm_selection(){

	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " C O N F I R M   S E L E C T I O N"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo ""

	console_message="'$p_name' ($p_pid); is process to watch? (Y)"

}

# confirm right program to watch
menu_process_confirm_selection(){

    case $user_choice in
        [Yy]* ) current_menu=$SELECT_PROFILE ;;
        * ) current_menu=$START ;;
    esac

}

# displays profile options
menu_show_choose_profile(){
	
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " C H O O S E   P R O F I L E"
	echo "~~~~~~~~~~~~~~~~~~~~~"

	echo " monitor utilization of: "
	echo "1. memory "
	echo "2. cpu"
	
	
	echo "~~"
	echo "b.  search once more "
	echo "~~"
	
	console_message="enter choice :"
	
	
}

# choose the top activity to display
menu_process_choose_profile(){

    case $user_choice in
        1) 
			profile_watch=$MEMORY 
			current_menu=$DISPLAY_PROFILE ;;
        2)
			profile_watch=$CPU
			current_menu=$DISPLAY_PROFILE ;;
			
		*) current_menu=$START ;;
    esac

}

# start the display
action_running_profile (){

	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " R U N N I N G "
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo ""

	# run it in background
	./loopForProcessProfile.sh &

	# wait for user
	pause
	
	# kill it
	kill %- 2>/dev/null
	
	# straight to menu to shortcut loop
	clear
	current_menu=$EXIT_REQUEST
	menu_show_exit_request

}

# exit step out
menu_show_exit_request(){

	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " E X I T  R E Q U E S T"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo ""

	console_message="exit the script? (Y)"

}

# if users says yes, then exit script set true
menu_process_exit_request(){

    case $user_choice in
        [Yy]* ) exit_script=1 ;;		
    esac
	current_menu=$START 

}


showMenus(){
	
	## echo "DEBUG: current_menu : $current_menu"
	clear
	case $current_menu in
		$START ) 			 request_process_name ;;
		$STRING_MATCHS ) 	 ensure_one_process_selected ;;
		$STRING_TO_PROCESS ) menu_show_find_process ;;
		$CONFIRM_CHOICE )	 menu_show_confirm_selection ;;
		$SELECT_PROFILE )	 menu_show_choose_profile ;;
		$DISPLAY_PROFILE ) 	 action_running_profile ;;
		$EXIT_REQUEST )		 menu_show_exit_request ;;
			
		* ) echo "error: menu display current menu is: $current_menu" ;;
	esac
}

read_options(){
	
	# get user choice
	read -p "$console_message" user_choice
	case "$current_menu" in
		
		$START) 
				name_string="$user_choice"
				ensure_one_process_selected
				;;
				
		$STRING_TO_PROCESS) menu_process_find_process ;;
		$CONFIRM_CHOICE)	menu_process_confirm_selection ;;
		$SELECT_PROFILE)	menu_process_choose_profile ;; 
		$EXIT_REQUEST)		menu_process_exit_request ;;
		
		
		## should not get here with this menu selected
		$DISPLAY_PROFILE)	echo "DEV ERROR AT DISPLAY_PROFILE"; exit 2;;
		
		*) echo -e "${RED}Error...${STD}" && sleep 2 ;;
	esac
}




# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MAIN
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# start on the start menu and loop til user says stop
current_menu=$START
while [[ exit_script -eq 0 ]] 
do
	showMenus
	read_options
done






#while true
#do
	#get users process name
#request_process_name
	

	# use ps and grup to return a matching list
	
	# if more than 10 records, get them to sharpen their pencil
	# use awk to convert this into a menu
		# add a go back opion to the bottom
	# use awk to match selection to an item - get PID/ name
	#  confirm selection
			# use go back here to


#done


	# ask user if they want memory or cpu
	# start background script
	
#pause


