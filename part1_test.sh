#!/bin/bash


# anthony brown s3460996

# this script reports system information

# funtions used in this script
FREE=/bin/free
# DF=/bin/df
# CAT=/bin/cat
# UPTIME=/bin/uptime


FLAG_OPTIONS="mdct"


############################################
# test parmeters

# test correct number of flags
if [[ $# -ne 1 ]]; then
        echo "Incorrect number of parameters."
        echo "This command takes only 1 argument flag:"
        echo "usage: $0 <-$FLAG_OPTIONS>" >&2
        exit 1
fi

flag_call=$1
flag_length=${#flag_call}

# test the correct length of the parameters
if [[ flag_length -ne 2 ]]; then
        echo "The parameter provided is the wrong length"
        echo "This command takes only 1 argument flag:"
        echo "usage: $0 <-$FLAG_OPTIONS>" >&2
        exit 1
fi

# get the flag value
flag_value=${flag_call:1:1}


###################################################
# main

case $flag_value in
	m)
		echo "memory  information ======"
		free -h
		
		echo "==========================" >&2
		;;
	d)
		echo "disk space information ==="
		df -h
		
		echo "==========================" >&2
		;;
	c)
		echo "network information ==="
		echo "SUMMARY"
		cat /etc/hosts
		
		echo "DETAILED"
		ip addr show
		
		echo "==========================" >&2
		;;
	t)
		echo "system run time =========="
		uptime
		
		echo "==========================" >&2
		;;
	*)
		echo "==unmatch parameter ========"
        	echo "This command takes only 1 argument flag:"
        	echo "usage: $0 <-$FLAG_OPTIONS>" >&2
		echo "============================" >&2
		;;
esac









