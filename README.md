# UNIX Systems Administration

## Undergraduate  RMI-CPT264

This readme contains a description of the UNIX subject and the work recorded in this repo. 

First up, I have added an outline the repo content. After that is a brief summary of the  course.


## GIT Repo Contents

__The short story__ : to run the functions in this repo, clone it then start  [menuSystem.sh](menuSystem.sh)


### Outline


 
This repo contains the output from assignments. Or more specifically, as explained  below,  this repo contains the scripts completed for assignment 2. 

For a simple outline of the assignment requirements please read [README_assignment_summary.md](README_assignment_summary.md). 

The assignment structure for the subject was a bit convoluted. 

I had to complete two assignments, where the second was submitted in three parts, so it seems more like 4 assignments. 

##### Assignment 1 : System Configuration

Assignment 1 addressed system setup, so the output isn't contained here (although some can be found on Docker). The assessor marked this assignment by installing from the docker link I provided, then ran some tests to see that it met their specifications.


##### Assignment 2 : UNIX Scripting

* Parts: 
	* A Kernel Compilation
	* B Scripting - report basic system information
	* C Scripting - advanced scripting Assignment 




### Details of contents
	
	
FILE | DETAILS 
----|--------
[README_ASSIGN2.md](README_ASSIGN2.md) | Usage of assignment 2 scripts
[README_assignment_summary.md](README_assignment_summary.md)  | summary of assignment requirements 
[*.docx files](assignment_specifications/) | original assignment specs (detailed)




## Course Notes
### Description
This subject provides a practical introduction to UNIX system administration using Linux. The Linux operating system, a free implementation of the UNIX system for personal computers, will be used extensively throughout this unit for lecture/tutorial examples and importantly during practical exercises. In addition to system administration proper, TCP/IP network service administration and the use of scripting languages will be introduced. As Linux implements the POSIX standard, most of the learning from this subject will be directly applicable to commercial UNIX systems.

### Topics covered
* UNIX Essentials
* Operating Systems
* Introduction to Scripting
* Intermediate Scripting
* Filters, Pattern Matching
* Disks and File Systems
* Installing Software
* The Boot Process
* Continuous Delivery
* Docker
* Task Automation