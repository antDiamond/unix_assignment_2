#!/bin/bash

# stop my spelling mistakes!
set -u


# A menu driven shell script sample template 
# addapted from from //bash.cyberciti.biz/guide/Menu_driven_scripts

#-----------------------------------
# Step: declarations
# ----------------------------------

#  menus
OPENING_MENU=1
FUNCTIONS_MENU=2
#FIND_MENU=3
#PROFILE_MENU=4



GREEN='\e[92m'
RED='\033[0;41;30m'
STD='\033[0;0;39m'

# drive menu selection
current_menu=$OPENING_MENU
user_choice=0


##  get references to external scripts 
FINDER=1
PROFILER=2

USAGE_FUNCTIONS=part1.sh
RUNNING_FUNCTIONS=part2.sh

FINDER_MENU=menuFinder.sh
PROFILER_MENU=menuProfiler.sh

flag_option=""
choose_external="$FINDER" 


##   test that external scripts have been found

test_functions_found(){

	local error_string=""

	if [ ! -f $USAGE_FUNCTIONS ]
	then
		error_string="$USAGE_FUNCTIONS $error_string" 
	fi

	if [ ! -f $RUNNING_FUNCTIONS ]
	then
		error_string="$RUNNING_FUNCTIONS $error_string" 
	fi

	if [ ! -f $FINDER_MENU ]
	then
		error_string="$FINDER_MENU $error_string" 
	fi
	
	if [ ${#error_string} != 0 ]
	then
		# report failed tests
		echo -e "${RED} Cannot find required files: $error_string ${STD} ">&2
		exit 2
	fi
}



#-----------------------------------
# Step: functions for output
# ----------------------------------
pause(){
 	echo -e "\n${GREEN}Press [Enter] key to continue...${STD}"
 	read -p "" throwAway
}


## run external cmd line driven scripts
# flag_option is set in the menu processor

run_part1(){
	eval ./"$USAGE_FUNCTIONS  -$flag_option"
	pause
}

run_part2(){
	eval ./"$RUNNING_FUNCTIONS  -$flag_option"
	pause
}


## run external function menu driven scripts
# 
run_external(){
	
	local source_program=""
	
	case $choose_external in
		$FINDER)
			source_program="$FINDER_MENU" ;;
				
		$PROFILER)
			source_program="$PROFILER_MENU"  ;; #TODO 
	esac
	
	clear
	eval ./"$source_program"
	clear
	## strange terminal display
	
	
	
}



# ----------------------------------------------
#  menus
# ----------------------------------------------
##    to altinate between menus
show_menus(){

	# echo "DEBUG: current_menu : $current_menu"
	case $current_menu in
		$OPENING_MENU) menu_1_show ;;
		$FUNCTIONS_MENU) menu_2_show ;;
		*) echo "error: menu display current menu is: $current_menu" ;;
	esac
}


# OPENING MENU 
# function to display menus
menu_1_show() {
	
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " M A I N - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. System information"
	echo "2. Find functionality"
	echo "3. program profiler"
	echo "~"
	echo "0. EXIT -(quit program)"
	echo ""
}

# FUNCTIONS MENU
menu_2_show(){
	
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "SYSTEM INFORMATION"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo ""
	echo "SYSTEM USAGE "
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. free and occupied memory on the system"
	echo "2. free and occupied disk space on the system"
	echo "3. connection information for each network connection"
	echo "4. the time the system has been running"
	echo ""
	echo "SYSTEM RUNNING"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "5. the number of CPU cores on the system"
	echo "6. the niceness of the current running process"
	echo "7. the current users count of running processes"
	echo "8. the current users' count of open file descriptors"
	echo "9. the default maximum allowable file descriptors"
	echo ""
	echo "~~~" 
	echo "0. RETURN (return to main menu)" 
	echo ""	
}

#menu_3_show(){
#	
#	echo "~~~~~~~~~~~~~~~~~~~~~"
#	echo "FINDER"
#	echo "~~~~~~~~~~~~~~~~~~~~~"
#	echo ""
#	echo "0. RETURN (return to previous menu)"
#}




# ----------------------------------------------
#  Read and process use input
# ----------------------------------------------

# read input from the keyboard 
# select processor based on menu level

read_options(){
	
	# get user choice
	read -p "Enter choice :" user_choice
	case $current_menu in
		$OPENING_MENU) menu_1_process ;;
		$FUNCTIONS_MENU) menu_2_process ;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}

# opening menu 
menu_1_process(){
	
	# choose cmd
	case $user_choice in
		1) current_menu=$FUNCTIONS_MENU ;;
		2) 
			choose_external="$FINDER"
			run_external
			;;
		3) 
		
			choose_external="$PROFILER"
			run_external
			;;
			
			##echo "selected 3, not functional at the moment" && sleep 2 ;;
		
		0) exit 0;;
		*) echo -e "${RED}Error...${STD}" && sleep 2 ;;
	esac
}
 
menu_2_process(){
	
	# choose cmd
	case $user_choice in
		1) flag_option=m run_part1;;
		2) flag_option=d run_part1;;
		3) flag_option=c run_part1;;
		4) flag_option=t run_part1;;

		5) flag_option=c run_part2;;
		6) flag_option=p run_part2;;
		7) flag_option=t run_part2;;
		8) flag_option=f run_part2;;
		9) flag_option=m run_part2;;
		
		0) current_menu=$OPENING_MENU ;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}

#OLD OLD
#menu_3_process(){
#	
#	# choose cmd
#	case $user_choice in
#		0) current_menu=$OPENING_MENU;;
#		*) echo -e "${RED}Error...${STD}" && sleep 2
#	esac
#}
 

 
# ----------------------------------------------
#  Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------

## TRAPS
##### TODO: the working trap
trap "" SIGINT SIGQUIT SIGTSTP
##### TODO: 
##### demo of function in trap: trap "rm -f $lockfile; exit" INT TERM EXIT


# -----------------------------------
#  Main logic 
# ------------------------------------


# confirm external file are found
test_functions_found


# infinite loop
while true
do
	clear
	show_menus
	read_options
done
