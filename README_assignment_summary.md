## Assignment 1 : System Configuration 

#### Complete  UNIX survival guide (10 marks)
>Go through the unix survival guide and explore the unix environment. Complete a quiz

#### Write How-to Guide (10 marks)
>You will need to construct a how-to guide to explain your steps to complete the assignment to a non-technical user.

#### Virtualization (10 marks)
>Install a virtualization environment (10 marks)
>Set up a virtualization environment. Install several operating systems to use for the remainder of the assignment.  
	
#### Ubuntu Requirements (15 Marks)
- Install Ubuntu 10 marks)
	- Install vim and gvim inside Ubuntu (5 marks)


#### Centos Requirements (20 marks)
- Install Centos
- Install Docker (10 marks) 
- install software into Docker container
  - gcc and g++ (3 marks)
  - emacs and xemacs (3 marks)
  - apache (3 marks)
  - sshd (3 marks)
  - python (4 marks)
  - You will also compile the fish shell (4 marks) 

#### Software Configuration (20 Marks)
- create a user in your docker container called “mrfishy”. 
- configure apache within the docker container 
- configure sshd 
- get emacs working over ssh (3 marks)
- setup docker so that your httpd server is accessible to the outside world 
- change system path for mrfishy so anaconda version of python runs 
	
#### Push your container to docker hub (5 Marks)
>You will need to create an account on docker hub and create a private repository.

#### Create a docker file that achieves all the above tasks (Bonus 5 marks)
>Your dockerfile will be used with docker to create a container with the specifications above.


## Assignment 2 : UNIX Scripting 
> this assignment is in parts which will be due sequentially. 
- Parts: 
	- A Kernel Compilation
	- B Scripting - report basic system information
	- C Scripting - advanced scripting


### Part A – Kernel Compilation 
>This component of the assignment requires you to compile and install a custom kernel. 

### Part B - Basic Scripting
#### Scripting without post proccessing (10 %)
- a script which, based on the argument passed in, provides:
	- the amount of free and occupied memory - formatted
	- the amount of disk space occupied and free - formatted
	- the connection information for each network connection 
	- The amount of time the system has been running. 
	
#### Scripting with post processing  (15%)
- write a bash script using getopts to receive the arguments that provides: 
	- the number of cpu cores on the system
	- the current process’ priority (nice number)
	- the total number of processes running under the current user
	- the number of open file descriptors owned by the current user
	- the maximum default number of  file descriptors that can be opened by a process. 

### Part C Scripting - advanced scripting

####  Find script (9%)

> You are to write a script that gets information from the user to run a query on the system using the find command. 

- You need to get from the user: 
	- the starting point (directory) where the search should start.
	- the thing to search for. This can be one of path, type, group, fstype
	- The value that is applied to the thing to search for. 
	- the maximum depth of the search 
	- The action to be taken on the search results 

#### Basic Profiler (9%)
- For this requirement you are to: 
	- prompt the user for the name (
	- partial matchs must be returned to a display a menu to refineasking
	- recomended (top, ps, grep, sed, awk)
- Next: 
	- ask the user whether they want to monitor memory or cpu utilization. 
	- run a background script that you have written that will print out once per second the utilization of the specified resource. 
	- when the user presses [enter] - terminate this background script and exit the foreground script as well. 

#### Menu System (7%)
- Create a script that provide a menu system 
	- you must disable ctrl-c (use trap) 
	- each level of menu must have a way to exit gracefully 
	- the whole script should only exit from the top level. 
	- Your menu must be well-presented and must be well-behaved
	- all inputs must be validated 
	- You may wish to clear the screen at each new menu.  








