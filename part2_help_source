.\" Process this file with
.\" groff -man -Tascii foo.1
.\"
.TH PART2 1 "OCTOBER 2018" Linux "User Manuals"
.SH NAME
part2.sh \- returns information about the system utilisation
.SH SYNOPSIS
.B part2.sh [option]
.SH DESCRIPTION
Provides details on the characteristics of the system and the current activity
being undertaken. You can return the number of CPUs, the nice number for the current
process, the total number of processes for the current user, the number of file 
descriptors the user has open and the current default settings for the maximum number of
file descriptors allowable. 

.SH OPTIONS
.IB -c 
the number of CPU cores on the system

.IB -p 
the niceness of the current running process

.IB -t 
the current users' count of running processes 

.IB -f 
the current users' count of open file descriptors

.BI -m
the default maximum allowable file descriptors


.SH ENVIRONMENT
These inbuild commands are used:
.IB /bin/ps, 
.IB /bin/nproc, 
.IB /bin/whoami, 
.IB /bin/lsof


.SH DIAGNOSTICS
The command will only take the flag options, but you many 
return multiples combinations, and in any order.

.SH AUTHOR
Anthony Brown s3460096 <s3460996@student.rmit.edu.au>
.SH "SEE ALSO"
.BR part1.sh(1)

