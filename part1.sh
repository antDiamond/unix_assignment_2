#!/bin/bash


# anthony brown s3460996

# this script reports system information

# funtions used in this script
# FREE=/bin/free
# DF=/bin/df
# CAT=/bin/cat
# UPTIME=/bin/uptime
# IP=/bin/ip


FLAG_OPTIONS="mdct"


############################################
# test parmeters

# test correct number of flags
if [[ $# -ne 1 ]]; then
        echo "Incorrect number of parameters."
        echo "This command takes only 1 argument flag:"
        echo "usage: $0 <-$FLAG_OPTIONS>" >&2
        exit 1
fi

flag_call=$1
flag_length=${#flag_call}

# test the correct length of the parameters
if [[ flag_length -ne 2 ]]; then
        echo "The parameter provided is the wrong length"
        echo "This command takes only 1 argument flag:"
        echo "usage: $0 <-$FLAG_OPTIONS>" >&2
        exit 1
fi

# get the flag value
flag_value=${flag_call:1:1}


###################################################
# main

case $flag_value in
	m)
		echo "===================================="
		echo "memory  information ======"
		echo "===================================="
		free -h
		
		echo "===================================="
		echo "==========================" >&2
		;;
	d)
		echo "===================================="
		echo "disk space information ==="
		echo "===================================="
		df -h
		
		echo "===================================="
		echo "==========================" >&2
		;;
	c)
		echo "===================================="
		echo "connection information ==="
		echo "===================================="
		echo
		echo "SUMMARY"
		echo
		cat /etc/hosts
		
		echo ""
		echo ""
		echo "DETAILED"
		echo ""
		ip addr show

		echo "===================================="
		echo "==========================" >&2
		;;
	t)
		echo "===================================="
		echo "system run time =========="
		echo "===================================="
		uptime
		
		echo "===================================="
		echo "==========================" >&2
		;;
	*)
		echo "===================================="
		echo "==unmatch parameter ========"
		echo "===================================="
        	echo "This command takes only 1 argument flag:"
        	echo "usage: $0 <-$FLAG_OPTIONS>" >&2
		echo "===================================="
		echo "============================" >&2
		;;
esac









